package com.training.northwind.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShipperTest {
    private static final String testCompanyName = "JUnit Test Company";
    private static final String testPhone = "+44 1234 9087 323";
    private Shipper shipper;

    @BeforeEach
    public void setup() {
        this.shipper = new Shipper();
    }

    @Test
    public void setTestCompanyName() {
        this.shipper.setCompanyName(testCompanyName);
        assertEquals(this.shipper.getCompanyName(), testCompanyName);
    }

    @Test
    public void setTestPhone() {
        this.shipper.setPhone(testPhone);
        assertEquals(this.shipper.getPhone(), testPhone);
    }


}
