package com.training.northwind.service;

import com.training.northwind.entity.Shipper;
import com.training.northwind.repository.ShipperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShipperService {

    @Autowired
    ShipperRepository shipperRepository;

    public List<Shipper> findAll() {
        return shipperRepository.findAll();
    }

    public Shipper findById(long id) {
        return shipperRepository.findById(id).get();
    }

    // just an interesting example of using the JPA interface to create new accessors.
    public List<Shipper> findByPhone(String phone) {
        return shipperRepository.findShipperByPhone(phone);
    }

    // upsert function (update and insert)
    public Shipper save(Shipper shipper) {
        return shipperRepository.save(shipper);
    }

    public void deleteById(long id) {
        shipperRepository.deleteById(id);
    }
}
