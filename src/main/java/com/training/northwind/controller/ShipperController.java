package com.training.northwind.controller;

import com.training.northwind.entity.Shipper;
import com.training.northwind.service.ShipperService;
import io.swagger.models.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/shipper")
public class ShipperController {

    @Autowired
    private ShipperService shipperService;

    @GetMapping
    public List<Shipper> findAll()
    {
        return shipperService.findAll();
    }
    @GetMapping("{id}")
    public ResponseEntity<Shipper> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<Shipper>(shipperService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @GetMapping("phone/{phone}")
    public List<Shipper> findByPhone(@PathVariable String phone) {
        return shipperService.findByPhone(phone);
    }

    @PostMapping
    public ResponseEntity<Shipper> create(@RequestBody Shipper shipper) {
        try {
            return new ResponseEntity<Shipper>(shipperService.save(shipper),HttpStatus.CREATED);
        } catch ( Exception e ) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping
    public ResponseEntity<Shipper> update(@RequestBody Shipper shipper) {
        try {
            return new ResponseEntity<Shipper>(shipperService.save(shipper),HttpStatus.OK);
        } catch ( Exception e ) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable long id) {
        try {
            shipperService.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }
}
